const csv=require('csvtojson');
const fs=require('fs');
const csvFilePathMatches='../data/matches.csv';
const csvFilePathDeliveries='../data/deliveries.csv';
csv()
.fromFile(csvFilePathMatches)
.then((jsonObj)=>{
fs.writeFileSync("../data/matches.json",JSON.stringify(jsonObj),"utf-8",(err)=>{  // converting matches.csv to matches.json in data directory
   if(err)
   console.log(err);
});
});
 
csv()
.fromFile(csvFilePathDeliveries)
.then((jsonObj)=>{
fs.writeFileSync("../data/deliveries.json",JSON.stringify(jsonObj),"utf-8",(err)=>{ // converting deliveries.csv to deliveries.json in data directory
   if(err)
   console.log(err);
});
});
 
