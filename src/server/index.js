const matches = require("../data/matches.json");
const deliveries = require("../data/deliveries.json");
const matchesPlayedPerYear = require("./matchesPlayedPerYear");
const matchesWonPerTeam = require("./matchesWonPerTeam");
const extraRun2016 = require("./extraRun2016").extraRun2016;
const top10EconomicalBowlers = require("./top10EconomicalBowlers");
const fs = require("fs");

/**
 * Creates a FileName.json file in the specified filePath.
 *
 * @param {String} filePath
 * @param {Object} FileName
 */
function writeJson(filePath, FileName) {
  fs.writeFile(filePath, JSON.stringify(FileName), (err) => {
    if (err) console.log(err);
  });
}

try {
  fs.mkdirSync("src/public/output"); // creating a empty output directory to store all the output files.
} catch (err) {}
const yearWithCount = matchesPlayedPerYear(matches);
writeJson("src/public/output/matchesPlayedPerYear.json", yearWithCount);

const eachTeamWinsPerYear = matchesWonPerTeam(matches);
writeJson("src/public/output/matchesWonPerTeam.json", eachTeamWinsPerYear);

const extraRunsPerTeam = extraRun2016(matches, deliveries);
writeJson("src/public/output/extraRun2016.json", extraRunsPerTeam);

const top10Bowlers = top10EconomicalBowlers(matches, deliveries);
writeJson("src/public/output/top10EconomicalBowlers.json", top10Bowlers);
