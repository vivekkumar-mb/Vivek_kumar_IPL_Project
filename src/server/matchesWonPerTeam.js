/**
 * Gives information about the IPL matches won, per team, per year.
 * 
 * @param {Object} matches - An object of matches.json file. 
 * @returns {Object} - matches won per team per year.
 */
const matchesWonPerTeam = (matches) => {

    

    return matches.reduce((eachTeamWins,entry) => {
        const year=entry.season;
        const winner=entry.winner;
        
        if (!eachTeamWins[year])
            eachTeamWins[year] = {};


        if (!eachTeamWins[year][winner]) {
            eachTeamWins[year][winner] = 1;
        } else {
            eachTeamWins[year][winner] += 1;
        }

        return eachTeamWins;

    },{});

    
}

module.exports = matchesWonPerTeam;