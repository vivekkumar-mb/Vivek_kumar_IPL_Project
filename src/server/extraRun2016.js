/**
 * Returns a list of matchId's in a given year.
 * 
 * @param {Object} matches -  An object of matches.json file. 
 * @param {string} parameterYear -  An object of deliveries.json file. 
 * @returns {Array} - Array of matchId's.
 */
const matchIdsYear = ( matches, parameterYear ) => { // this function returns an Array of all matchIds of a given year.

    return  matches.reduce( ( matchIdsArray, entry ) => {
        const year = entry.season;
        const ids = entry.id;

        if ( year == parameterYear )
            matchIdsArray.push( ids );

        return matchIdsArray;
    }, [] );

}

/**
 * Gives information about Extra runs conceded per team in the year 2016.
 * 
 * @param {Object} matches -  An object of matches.json file.
 * @param {Object} deliveries - An object of deliveries.json file.
 * @returns {Object} - Extra runs conceded per team in the year 2016
 */


const extraRun2016 = ( matches, deliveries ) => {
    const validIds = matchIdsYear( matches, '2016' );

    return  deliveries.reduce( ( extraRunPerTeam, entry ) => {
        const matchId = entry[ 'match_id' ];
        const bowlingTeam = entry[ 'bowling_team' ];
        const extraRuns = parseInt( entry[ 'extra_runs' ] );

        if ( validIds.includes( matchId ) ) {

            if ( !extraRunPerTeam[ bowlingTeam ] )
                extraRunPerTeam[ bowlingTeam ] = extraRuns;
            else
                extraRunPerTeam[ bowlingTeam ] += extraRuns;

        }
        return extraRunPerTeam;
    }, {} );


}





module.exports = {
    extraRun2016,
    matchIdsYear
};