/**
 * Gives information about bowlers with the total runs they got hit, in year 2015.
 * 
 * @param {Object} matches - An object of matches.json file.
 * @param {Object} deliveries - An object of deliveries.json file.
 * @returns {Object} - bowlers with the total runs they got hit, in year 2015.
 */
const bowlersWithTotalRuns = ( matches, deliveries ) => { //this function returns bowlers with the total number of runs they got hit in year 2015
    const matchIdsYear = require( './extraRun2016' );
    const validIds = matchIdsYear.matchIdsYear( matches, '2015' );


    return deliveries.reduce( ( bowlersWithRuns, entry ) => {
        if ( validIds.includes( entry[ 'match_id' ] ) ) {

            const bowler = entry[ 'bowler' ];

            const runsPerBall = parseInt( entry[ 'wide_runs' ] ) +
                parseInt( entry[ 'noball_runs' ] ) +
                parseInt( entry[ 'batsman_runs' ] );

            if ( !bowlersWithRuns[ bowler ] )

                bowlersWithRuns[ bowler ] = runsPerBall;

            else

                bowlersWithRuns[ bowler ] += runsPerBall;
        }
        return bowlersWithRuns;
    }, {} );


}


/**
 * Gives information about bowlers with the total Overs they bowled, in year 2015.
 * 
 * @param {Object} matches - An object of matches.json file.
 * @param {Object} deliveries - An object of deliveries.json file.
 * @returns {Object} - bowlers with the total Overs they bowled, in year 2015.
 */
const bowlersWithTotalOvers = ( matches, deliveries ) => { //this function returns bowlers with there total number of Overs bowled in year 2015.
    const matchIdsYear = require( './extraRun2016' );

    const validIds = matchIdsYear.matchIdsYear( matches, '2015' );



    const bowlersWithOvers = deliveries.reduce( ( bowlersWithOvers, entry ) => {

        if ( validIds.includes( entry[ 'match_id' ] ) ) {

            const bowler = entry[ 'bowler' ];

            const matchId = entry[ 'match_id' ];

            const overNumber = parseInt( entry[ 'over' ] );

            const matchWithOver = matchId + " " + overNumber;


            if ( !bowlersWithOvers[ bowler ] )

                bowlersWithOvers[ bowler ] = [ matchWithOver ];


            if ( !bowlersWithOvers[ bowler ].includes( matchWithOver ) )

                bowlersWithOvers[ bowler ].push( matchWithOver );
        }
        return bowlersWithOvers;
    }, {} );

    return Object.entries( bowlersWithOvers ).map( player => {

        player[ 1 ] = player[ 1 ].length;
        return player;
    } ).reduce( ( bowlersWithOvers, array ) => {
        bowlersWithOvers[ array[ 0 ] ] = array[ 1 ];
        return bowlersWithOvers
    }, {} );


}

/**
 * Gives information about Top 10 economical bowlers in the year 2015
 * 
 * @param {Object} matches - An object of matches.json file.
 * @param {Object} deliveries - An object of deliveries.json file.
 * @returns {Object} - Top 10 economical bowlers in the year 2015
 */

const top10EconomicalBowlers = ( matches, deliveries ) => { // this function returns top10EconomicalBowlers
    const bowlersWithRuns = bowlersWithTotalRuns( matches, deliveries );

    const bowlersWithOvers = bowlersWithTotalOvers( matches, deliveries );

    const bowlersEconomy = {};

    for ( let player in bowlersWithRuns ) {

        bowlersEconomy[ player ] = bowlersWithRuns[ player ] / bowlersWithOvers[ player ];

    }

    const sortedBowlerEconomy = Object // this returns Bowlers sorted by there respective economy.
        .entries( bowlersEconomy )
        .sort( ( a, b ) => a[ 1 ] - b[ 1 ] )
        .reduce( ( _sortedObj, [ k, v ] ) => ( {
            ..._sortedObj,
            [ k ]: v
        } ), {} );

    let count = 0;
    const top10Bowlers = {};
    for ( bowler in sortedBowlerEconomy ) { // this selects top10 Bowlers sorted by there respective economy. 
        top10Bowlers[ bowler ] = parseFloat( sortedBowlerEconomy[ bowler ].toFixed( 2 ) );
        count++;
        if ( count === 10 )
            break;
    }
    return top10Bowlers; // returning top10 bowlers
}


module.exports = top10EconomicalBowlers;