/** Gives information about the number of IPL matches played per year.
 * 
 * @param {Object} matches - An object of matches.json file.
 * @returns {Object} - Ipl matches played per year.
 */
const matchesPlayedPerYear = (matches) => {

    

    const yearWithCount =  matches.reduce((yearWithCount,entry) => {
        const year=entry.season;

        if (yearWithCount[year]===undefined) {

            yearWithCount[year] = 1;

        } else {

            yearWithCount[year] += 1;

        }
        return yearWithCount;
    },{});

    return yearWithCount;
}
module.exports = matchesPlayedPerYear;